<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {
    Route::get('{address}/{contract_name}/{token_id}', 'MetadataController@show')->name('matadata.show');
    Route::post('metadata/store', 'MetadataController@store')->name('matadata.store');
});
