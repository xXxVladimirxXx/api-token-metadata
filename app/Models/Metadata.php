<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Metadata extends Eloquent
{
    protected $connection = 'mongodb';

    protected $collection = 'metadata';

    protected $hidden = ['_id', 'address', 'contract_name', 'token_id', 'created_at', 'updated_at'];

    protected $fillable = [
        'name',
        'cover_image',
        'source_file',
        'external_url',
        'description',
        'attributes',

        // To identify the token
        'address',
        'contract_name',
        'token_id'
    ];
}
