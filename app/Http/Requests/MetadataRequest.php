<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class MetadataRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' =>           'required|string',
            'cover_image' =>    'required|string',
            'source_file' =>    'required|string',
            'external_url' =>   'string',
            'description' =>    'required|string',
            'attributes' =>     'array',

            // To identify the token
            'address' =>        'required|string',
            'contract_name' =>  'required|string',
            'token_id' =>       'required|numeric'
        ];
    }

}
