<?php

namespace App\Http\Controllers\Api;

use App\Models\Metadata;
use Illuminate\Http\Request;
use App\Http\Requests\MetadataRequest;
use Illuminate\Support\Facades\Validator;

class MetadataController extends Controller
{
     /**
    * @OA\Get(
    *     path="{address}/{contract_name}/{token_id}",
    *     summary="Method for obtaining metadata by the address of the account of the owner of the smart contract, the name of the smart contract and the token id",
    *     tags={"show"},
    *     @OA\Parameter(name="address", in="path", required=true, description="type=`string` | description=`Account owner address where the smart contract is located`"),
    *     @OA\Parameter(name="contract_name", in="path", required=true, description="type=`string` | description=`Smart contract name`"),
    *     @OA\Parameter(name="token_id", in="path", required=true, description="type=`integer` | description=`Token ID`"),
    *     @OA\Response(
    *         response=200,
    *         description="Metadata was found",
    *         @OA\Schema(
    *             type="string"
    *         )
    *     )
    * )
    */
    public function show($address, $contract_name, $token_id) {
        return response()->json(Metadata::where(['address' => $address, 'contract_name' => $contract_name, 'token_id' => (int) $token_id])->first(), 200);
    }

     /**
    * @OA\Post(
    *     path="metadata/store",
    *     summary="Method for creating new metadata",
    *     tags={"store"},
    *     @OA\Parameter(name="name", in="path", required=true, description="type=`string` | description=`Token name`"),
    *     @OA\Parameter(name="cover_image", in="path", required=true, description="type=`string` | description=`Path to token cover image`"),
    *     @OA\Parameter(name="source_file", in="path", required=true, description="type=`string` | description=`Path to the main token file`"),
    *     @OA\Parameter(name="external_url", in="path", required=false, description="type=`string` | description=`Link to the token display page`"),
    *     @OA\Parameter(name="description", in="path", required=true, description="type=`string` | description=`Token Description`"),
    *     @OA\Parameter(name="attributes", in="path", required=false, description="type=`array` | description=`Token attributes. Each attribute must have three fields {attribute_template, attribute_type, attribute_value}`"),
    *     @OA\Parameter(name="address", in="path", required=true, description="type=`string` | description=`Account owner address where the smart contract is located`"),
    *     @OA\Parameter(name="contract_name", in="path", required=true, description="type=`string` | description=`Smart contract name`"),
    *     @OA\Parameter(name="token_id", in="path", required=true, description="type=`integer` | description=`Token ID`"),
    *    @OA\Response(
    *         response=200,
    *         description="Metadata has been successfully created",
    *         @OA\Schema(
    *             type="string"
    *         )
    *     ),
    *     @OA\Response(
    *         response=422,
    *         description="An error will occur if the required parameters were not passed. A list of those data that was not transmitted will return",
    *         @OA\Schema(
    *             type="string"
    *         )
    *     )
    * )
    */
    public function store(MetadataRequest $request) {
        $this->validateAttributes($request->toArray());
        return response()->json(Metadata::create($request->toArray()), 200);
    }

    private function validateAttributes($requestArray) {
        if (isset($requestArray['attributes'])) {
            $validator = Validator::make($requestArray['attributes'], [ "*.attribute_template" => 'required|string', "*.attribute_type" => 'required|string', "*.attribute_value" => 'required']);
            if ($validator->fails()) die(json_encode($validator->failed()));
        }
    }
}
